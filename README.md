# Processo Seletivo Hepta UX

Essa avaliação técnica contempla um teste prático. Abaixo as explicações e informações necessárias para a entrega.

É preciso que você crie um protótipo de uma parte de uma proposta de solução para uma das áreas internas da empresa. O protótipo deve ser baseado no Manual de Identidade Visual que está na raiz do projeto (HEPTA DIGITAL.PDF).

O caso é fictício, apenas para auxiliar na avaliação.


# BRIEFING 

A Hepta está criando uma nova plataforma para seus colaboradores. Nessa plataforma, os colaboradores vão poder ver as parcerias da empresa (lista descrita abaixo), os aniversariantes do dia e vão poder atualizar suas informações de cadastro (endereço, telefone, email para contato e dependentes). 
As informações de cadastro atualizadas vão precisar passar por uma área de aprovação. Essa área só poderá ser acessada pelo pessoal de RH. Ela deverá ter uma função para aprovar ou reprovar a atualização solicitada pelo colaborador.

# DESAFIO
Sua atribuição neste momento é criar o protótipo navegável. Com ele, o seu cliente interno precisará compreender qual será a proposta gráfica e verificar se as funcionalidades propostas (botões, localização de menu, localização de textos e imagens, iconografia, etc) estarão adequadas à experiência do usuário final.
O protótipo deve ter a visualização dos colaboradores, a área administrativa para aprovar ou reprovar as atualizações de cadastro e uma forma para cadastrar novas parcerias. Fique à vontade para criar uma versão web, mobile ou os dois.

Você deverá entregar um ou mais links para um protótipo navegável com indicações de botões sem a necessidade de animação. O prazo para a entrega deste trabalho é 5 dias úteis.

# LISTA DE PARCERIAS
- Atendimento Psicológico<br/>
Atendimento para toda nossa equipe por um preço especial de R$30,00/sessão online.<br/>

- IESB Centro Universitário<br/>
Corre que o desconto é pra você e pra sua família!<br/>

- Instituto Guilarducci – Psicoterapia E Avaliação Psicológica<br/>
Parceria para incentivar cuidados à saúde mental dos colaboradores<br/>

- Centro De Acupuntura Shen<br/>
Desconto para o #TimeHepta conhecer os benefícios da Medicina Tradicional Chinesa<br/>

- Restaurante Sabor Mineiro<br/>
Restaurante próximo à Feira dos Importados<br/>

- TI.Exames<br/>
Desconto de 10% em treinamentos E-learning

# O que fazer agora

Ao terminar envie o protótipo para gustavo.oliveira@hepta.com.br com o título "Processo seletivo - [seu nome]".

# Avaliação

A partir da entrega do seu trabalho, o gestor da vaga avaliará:<br/> 
•    Criatividade<br/>
•    Foco no usuário <br/>
•    Recursos que enriqueçam o design e a experiência<br/>
•    Domínio da ferramenta<br/>
•    Bom senso estético e conhecimento da teoria de cores<br/>
•    Conhecimento extenso em padrões de design para web e mobile.
